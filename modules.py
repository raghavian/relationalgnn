import torch
import torch.nn as nn
import torch.nn.functional as F
import math

SIGMA = 1
EPSILON = 1e-5


class EdgeGNN(nn.Module):
    def __init__(self, nfeat, nhid, dropout):
        super(EdgeGNN, self).__init__()

        self.fc_node_1_1 = nn.Linear(nfeat, nhid)
        self.fc_node_1_2 = nn.Linear(nhid, nhid)

        self.fc_edge_1_1 = nn.Linear(nhid * 2, nhid)
        self.fc_edge_1_2 = nn.Linear(nhid, nhid)

        self.fc_node_2_1 = nn.Linear(nhid * 3, nhid)
        self.fc_node_2_2 = nn.Linear(nhid, nhid)

        self.fc_edge_2_1 = nn.Linear(nhid * 3, nhid)
        self.fc_edge_2_2 = nn.Linear(nhid, nhid)

        self.dec = nn.Linear(nhid,1)
        self.dropout = dropout

    def encode(self, x, n2e_in, n2e_out):

        # Node MLP
        x = F.relu(self.fc_node_1_1(x))
        x = F.dropout(x, self.dropout, training=self.training)
        x = F.relu(self.fc_node_1_2(x))

        # Node to edge
        x_in = SparseMM()(n2e_in, x)
        x_out = SparseMM()(n2e_out, x)
        x = torch.cat([x_in, x_out], 1)

        # Edge MLP
        x = F.relu(self.fc_edge_1_1(x))
        x = F.dropout(x, self.dropout, training=self.training)
        x = F.relu(self.fc_edge_1_2(x))

        return x

    def forward(self, inputs, n2e_in, n2e_out, x_idx, y_idx):
        z = self.encode(inputs, n2e_in, n2e_out)
        preds = F.sigmoid(self.dec(z))
        return preds.view(-1), z



class SparseMM(torch.autograd.Function):
    """
    Sparse x dense matrix multiplication with autograd support.

    Implementation by Soumith Chintala:
    https://discuss.pytorch.org/t/
    does-pytorch-support-autograd-on-sparse-matrix/6156/7
    """

    def forward(self, matrix1, matrix2):
        self.save_for_backward(matrix1, matrix2)
        return torch.mm(matrix1, matrix2)

    def backward(self, grad_output):
        matrix1, matrix2 = self.saved_tensors
        grad_matrix1 = grad_matrix2 = None

        if self.needs_input_grad[0]:
            grad_matrix1 = torch.mm(grad_output, matrix2.t())

        if self.needs_input_grad[1]:
            grad_matrix2 = torch.mm(matrix1.t(), grad_output)

        return grad_matrix1, grad_matrix2



