from __future__ import division
from __future__ import print_function

import time
import argparse

import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable

from utils import *
from modules import EdgeGNN
import pdb

gamma = 0.5 # Gamma for Focal CE

# Training settings
parser = argparse.ArgumentParser()
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='Disables CUDA training.')
parser.add_argument('--seed', type=int, default=42, help='Random seed.')
parser.add_argument('--dataset', type=str, default='gnn',
                    help='Path to training data file.')
parser.add_argument('--epochs', type=int, default=5000,
                    help='Number of epochs to train.')
parser.add_argument('--lr', type=float, default=0.001,
                    help='Initial learning rate.')
parser.add_argument('--hidden', type=int, default=256,
                    help='Number of hidden units.')
parser.add_argument('--dropout', type=float, default=0.,
                    help='Dropout rate (1 - keep probability).')
parser.add_argument('--weight_decay', type=float, default=0.,
                    help='Weight decay (L2 loss on parameters).')
parser.add_argument('--gpu', type=int, default=0,
                    help='GPU number.')
parser.add_argument('--batch', type=int, default=1,
                    help='Size of mini-batch.')
parser.add_argument('--train_num', type=int, default=0,
                    help='Size of training set.')
parser.add_argument('--model', type=str,
                    help='Path to pre-trained model.')
parser.add_argument('--preTrain', action='store_true', default=False,
                    help='Use Pretrained model')
parser.add_argument('--save', action='store_true', default=False,
                    help='Save trained model')
parser.add_argument('--weight', type=float, default=0.5,
                    help='Weighting for wBCE.')


args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()
mini = args.batch
weight = args.weight

np.random.seed(args.seed)
torch.manual_seed(args.seed)
if args.cuda:
    torch.cuda.manual_seed(args.seed)

print("Loading and preprocessing training data...")
#Input: Nodes in NxF matrix (nodes), NxN dense adjacency matrix (adj_dense)
nodes = np.loadtxt('feat_mat.csv', delimiter = ",")
adj_dense = np.loadtxt('adj_mat.csv', delimiter = ",")

features = np.array(np.transpose(nodes), dtype=np.float32)

adj = sp.csr_matrix(adj_dense, dtype=np.float32)

# Node to edge transformer matrices
n2e_in = sp.csr_matrix((np.ones(adj.nnz),
                        (np.arange(adj.nnz), sp.find(adj)[1])),
                       shape=(adj.nnz, adj.shape[0]))
n2e_out = sp.csr_matrix((np.ones(adj.nnz),
                         (np.arange(adj.nnz), sp.find(adj)[0])),
                        shape=(adj.nnz, adj.shape[0]))

# Normalize
features = (features - features.min(0)) * 2 / \
           (features.max(0) - features.min(0)) - 1
adj_dense = row_normalize(adj_dense)

adj = sp.csr_matrix(adj_dense, dtype=np.float32)
# adj_target = sp.csr_matrix(adj_target, dtype=np.float32)

num_nodes = adj.shape[0]

idx_pos = to_linear_idx(sp.find(adj_target)[0],
                        sp.find(adj_target)[1],
                        num_nodes)
idx_neg = to_linear_idx(sp.find(adj)[0], sp.find(adj)[1], num_nodes)

idx_neg = np.setdiff1d(idx_neg, idx_pos)

features = torch.FloatTensor(features)
adj = sparse_mx_to_torch_sparse_tensor(adj)
n2e_in = sparse_mx_to_torch_sparse_tensor(n2e_in)
n2e_out = sparse_mx_to_torch_sparse_tensor(n2e_out)

adj_flat = np.array(adj.todense(), dtype=np.float32).reshape(-1) #Originally adj_target
adj_flat = torch.FloatTensor(adj_flat)

idx_all = np.hstack((idx_pos, idx_neg))
idx_all = np.array(idx_all, dtype=np.int64)
x_idx_all, y_idx_all = to_2d_idx(idx_all, num_nodes)

idx_all = torch.LongTensor(idx_all)
x_idx_all = torch.LongTensor(x_idx_all)
y_idx_all = torch.LongTensor(y_idx_all)

adj_flat = adj_flat[idx_all]

if (self.cuda):
    features = features.cuda(self.gpu)
    adj_flat = adj_flat.cuda(self.gpu)
    idx_all = idx_all.cuda(self.gpu)
    x_idx_all = x_idx_all.cuda(self.gpu)
    y_idx_all = y_idx_all.cuda(self.gpu)
    n2e_in = n2e_in.cuda(self.gpu)
    n2e_out = n2e_out.cuda(self.gpu)

print("Loading and preprocessing validation data...")
#Load a subset of the graph as validation data here !!!
# Let's say your validation data is in these csv files
nodes = np.loadtxt('feat_mat_vl.csv', delimiter = ",")
adj_dense = np.loadtxt('adj_mat_vl.csv', delimiter = ",")

features = np.array(np.transpose(nodes), dtype=np.float32)

adj = sp.csr_matrix(adj_dense, dtype=np.float32)

# Node to edge transformer matrices
n2e_in = sp.csr_matrix((np.ones(adj.nnz),
                        (np.arange(adj.nnz), sp.find(adj)[1])),
                       shape=(adj.nnz, adj.shape[0]))
n2e_out = sp.csr_matrix((np.ones(adj.nnz),
                         (np.arange(adj.nnz), sp.find(adj)[0])),
                        shape=(adj.nnz, adj.shape[0]))

# Normalize
features = (features - features.min(0)) * 2 / \
           (features.max(0) - features.min(0)) - 1
adj_dense = row_normalize(adj_dense)

adj = sp.csr_matrix(adj_dense, dtype=np.float32)
# adj_target = sp.csr_matrix(adj_target, dtype=np.float32)

num_nodes = adj.shape[0]

idx_pos = to_linear_idx(sp.find(adj_target)[0],
                        sp.find(adj_target)[1],
                        num_nodes)
idx_neg = to_linear_idx(sp.find(adj)[0], sp.find(adj)[1], num_nodes)

idx_neg = np.setdiff1d(idx_neg, idx_pos)

featuresVl = torch.FloatTensor(features)
adjVl = sparse_mx_to_torch_sparse_tensor(adj)
n2e_inVl = sparse_mx_to_torch_sparse_tensor(n2e_in)
n2e_outVl = sparse_mx_to_torch_sparse_tensor(n2e_out)

adj_flat = np.array(adj.todense(), dtype=np.float32).reshape(-1) #Originally adj_target
adj_flat = torch.FloatTensor(adj_flat)

idx_all = np.hstack((idx_pos, idx_neg))
idx_all = np.array(idx_all, dtype=np.int64)
x_idx_all, y_idx_all = to_2d_idx(idx_all, num_nodes)

idx_allVl = torch.LongTensor(idx_all)
x_idx_allVl = torch.LongTensor(x_idx_all)
y_idx_allVl = torch.LongTensor(y_idx_all)

adj_flatVl = adj_flat[idx_allVl]

if (self.cuda):
    featuresVl = featuresVl.cuda(self.gpu)
    adj_flatVl = adj_flatVl.cuda(self.gpu)
    idx_allVl = idx_allVl.cuda(self.gpu)
    x_idx_allVl = x_idx_allVl.cuda(self.gpu)
    y_idx_allVl = y_idx_allVl.cuda(self.gpu)
    n2e_inVl = n2e_inVl.cuda(self.gpu)
    n2e_outVl = n2e_outVl.cuda(self.gpu)

feature_shape = nodes.shape[1]
print("Training model...")

# Model and optimizer
model = EdgeGNN(nfeat=feature_shape,
                nhid=args.hidden,
                dropout=args.dropout)
if(args.preTrain):
    model.load_state_dict(torch.load(args.model))
    print("Loaded pre-trained model...")
optimizer = optim.Adam(model.parameters(),
                       lr=args.lr, weight_decay=args.weight_decay, amsgrad=True)


def binary_accuracy(output, labels):
    preds = output > 0.5
    correct = preds.type_as(labels).eq(labels).double()
    correct = correct.sum()
    return correct / len(labels)

def evaluate():

    if args.cuda:
        model.cuda(args.gpu)

    features = Variable(featuresVl)
    model.eval()

    preds, embeddings = model(featuresVl, n2e_inVl, n2e_outVl, x_idx_allVl,
                              y_idx_allVl)

    loss_val = F.binary_cross_entropy(preds, adj_flatVl)

    acc_val = binary_accuracy(preds, adj_flatVl)

    dice_val = dice(preds, adj_flatVl)

    return loss_val.item(), acc_val.item(), dice_val.item()

def train(epoch):

    if args.cuda:
        model.cuda(args.gpu)

    features = Variable(features)

    preds, embeddings = model(features, n2e_in, n2e_out, x_idx_all,
                              y_idx_all)

    loss_train = F.binary_cross_entropy(preds, adj_flat)
    loss_tr = loss_train.item()

    acc_train = binary_accuracy(preds, adj_flat)
    acc_tr = acc_tr.item()

    dice_train = dice(preds, adj_flat)
    dice_tr = dice_train.item()

    loss_train.backward()
    optimizer.step()

    loss_val, acc_val, dice_val = evaluate()

    print('Epoch: {:04d}'.format(epoch + 1),
          'loss_train: {:.4f}'.format(loss_train.item()),
          'acc_train: {:.4f}'.format(acc_train.item()),
          'dice_train: {:.4f}'.format(dice_train.item()),
          'loss_val: {:.4f}'.format(loss_val),
          'acc_val: {:.4f}'.format(acc_val),
          'dice_val: {:.4f}'.format(dice_val),
          'time: {:.4f}s'.format(time.time() - t))
    return loss_train.item(), dice_train.item(), loss_val, dice_val

# Train model
t_total = time.time()
loss_tr = np.zeros(args.epochs)
loss_vl = np.zeros(args.epochs)
dice_tr = np.zeros(args.epochs)
dice_vl = np.zeros(args.epochs)

for epoch in range(args.epochs):
    loss_tr[epoch], dice_tr[epoch], loss_vl[epoch], dice_vl[epoch] = train(epoch)

print("Optimization Finished!")
print("Total time elapsed: {:.4f}s".format(time.time() - t_total))

